# Introduction

This repository has the code needed to implement a PuWi Player using an ESP8266 WiFi enabled MCU. You can pair one or more PuWi Players to a [PuWi Host](https://gitlab.com/doragasu/puwi_host) to make live quiz games.

# Building

You will need to install version 3.4 of the [ESP8266\_RTOS\_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) to build the sources. After installing and configuring the SDK, plug the target board, clone the sources build them and flash the binary:


```Bash
$ git clone --recursive https://gitlab.com/doragasu/puwi_player.git
$ cd puwi_player
$ make menuconfig
$ make -j$(($(nproc)+1)) flash
```

Optionally, before the build, there are some items you can configure using `make menuconfig`:

* `Example Configuration`: Here you can set the SSID and password of the PuWi Host. Make sure they match or players will not be able to connect.
* `Component Config / Log Output / Default log verbosity`: It is recommended to set this to `Error` to reduce latency.

# I/O Wiring

* **GPIO13**: Connect the pushbuton to this pin. Code expects this button to be active low.
* **GPIO12** (optional): Connect the input of an array of 8 WS2812 RGB LEDs.
* **GPIO5** (optional): Connect the pushbutton LED. It is recommended to use a transistor such as a 2N7002 to be able to supply enough current to the LED.
* **A0** (analog pin, optional): Wire to the battery positive pin. Note this requires a resistor divider using e.g. a 330k and a 100k resistor.

Note that other than these pins, you will also have to wire the power and ground pins to the pushbutton and LEDs as needed.

# Usage

Power the board on. If you have connected the WS2812 LEDs, they should start pulsing in purple color, indicating that PuWi player is trying to connect to a PuWi Host nearby. Once connected, the LEDs should start doing the slide animation. If a color and LED intensity has been configured for this PuWi Player by the Host, the LEDs will change color and intensity as configured.

For more information, refer to the [PuWi Host](https://gitlab.com/doragasu/puwi_host) repository.

# Author

This project has been built and coded by Jesús Alonso (doragasu).

# License

The code in this repository comes with NO WARRANTY and is provided under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
