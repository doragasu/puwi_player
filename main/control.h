#ifndef __CONTROL_H__
#define __CONTROL_H__

#include <stdint.h>
#include <lwip/udp.h>

#include "battery.h"

enum cfg_color {
	CFG_COLOR_RED = 0,
	CFG_COLOR_GREEN,
	CFG_COLOR_BLUE,
	CFG_COLOR_YELLOW,
	__CFG_COLOR_MAX
};

enum cfg_intensity {
	CFG_INTENSITY_0 = 0,
	CFG_INTENSITY_20,
	CFG_INTENSITY_40,
	CFG_INTENSITY_60,
	CFG_INTENSITY_80,
	CFG_INTENSITY_100,
	__CFG_INTENSITY_MAX
};

// Network related events are placed first, so they have the same id as the
// ones defined in player_cmd.h in host project.
#define CTRL_EVENT_TABLE(X_MACRO) \
	X_MACRO(START, start) \
	X_MACRO(BLOCK, block) \
	X_MACRO(TRIGGER_ACK, trigger_ack) \
	X_MACRO(COLOR, color) \
	X_MACRO(BUTTON, button) \
	X_MACRO(IP_GET, ip_get) \
	X_MACRO(BATTERY_LEVEL, battery_level) \

#define X_AS_CTRL_EVENT_TYPE(u_name, l_name) CTRL_EVENT_ ## u_name,
enum ctrl_event_type {
	CTRL_EVENT_TABLE(X_AS_CTRL_EVENT_TYPE)
	__CTRL_EVENT_MAX
};

struct ctrl_led {
	enum cfg_color color;
	enum cfg_intensity intensity;
};

struct ctrl_battery_level {
	enum bat_event event;
	enum bat_status status;
	uint32_t voltage_millis;
};

union ctrl_data {
	uint32_t gpio;
	struct {
		uint32_t player_num;
		struct ctrl_led led;
		struct ctrl_battery_level battery_level;
	};
	ip4_addr_t server_ip;
};

struct ctrl_event {
	enum ctrl_event_type event;
	union ctrl_data data;
};

#define HOST_MSG_TABLE(X_MACRO) \
	X_MACRO(START_ACK, start_ack) \
	X_MACRO(TRIGGER, trigger) \
	X_MACRO(BLOCK_ACK, block_ack) \
	X_MACRO(BATT_LOW_CHARGE, batt_low_charge) \
	X_MACRO(BATT_LOW_DISCHARGE, batt_low_discharge) \
	X_MACRO(BATT_MID_CHARGE, batt_mid_charge) \
	X_MACRO(BATT_MID_DISCHARGE, batt_mid_discharge) \
	X_MACRO(BATT_HIGH_CHARGE, batt_high_charge) \
	X_MACRO(BATT_HIGH_DISCHARGE, batt_high_discharge) \

#define X_AS_HOST_MSG_TYPE(u_name, l_name) HOST_MSG_ ## u_name,
enum ctrl_host_msg_type {
	HOST_MSG_TABLE(X_AS_HOST_MSG_TYPE)
	__HOST_MSG_MAX
};

struct ctrl_host_msg {
	enum ctrl_host_msg_type event;
	uint8_t bssid[6];
};

void ctrl_init(struct udp_pcb *socket, const uint8_t bssid[6]);
int ctrl_start(void);
int ctrl_event(const struct ctrl_event *event);
void ctrl_event_from_isr(const struct ctrl_event *event);

#endif /*__CONTROL_H__*/

