#ifndef _WIFI_STA_H_
#define _WIFI_STA_H_

#include <stdint.h>
#include <lwip/ip4.h>
#include <esp_wifi.h>

struct wifi_event_assoc {
	uint8_t bssid[6];
};

struct wifi_event_disassoc {
	uint8_t bssid[6];
};

struct wifi_event_got_ip {
	tcpip_adapter_ip_info_t ip_info;
};

union wifi_event_data {
	struct wifi_event_assoc assoc;
	struct wifi_event_disassoc disassoc;
	struct wifi_event_got_ip got_ip;
};

enum wifi_event {
	WIFI_EVENT_ASSOC,
	WIFI_EVENT_DISASSOC,
	WIFI_EVENT_GOT_IP,
	__WIFI_EVENT_MAX
};

typedef void (*wifi_event_cb)(enum wifi_event event,
		union wifi_event_data *data);

void wifi_init_sta(wifi_event_cb event_cb);

#endif /*_WIFI_STA_H_*/

