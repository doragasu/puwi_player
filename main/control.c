#include <string.h>
#include <FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <freertos/timers.h>
#include <lwip/udp.h>
#include <driver/gpio.h>

#include <yamcha.h>

#include "control.h"
#include "util.h"

#define HOST_PORT 777
#define RETRY_TOUT_MS pdMS_TO_TICKS(100)

#define CTRL_TASK_PRIO	(tskIDLE_PRIORITY + 1)
#define CTRL_TASK_STACK	2048

#define LED_PIN 5
#define LED_PIN_MASK (1UL<<LED_PIN)

#define LED_ON 1
#define LED_OFF 0

// By default, no color defined
#define CFG_COLOR_DEFAULT __CFG_COLOR_MAX

#define CFG_INTENSITY_DEFAULT CFG_INTENSITY_60

#define SCALE_U8(value, scale) (((uint_fast16_t)(value) * \
			(uint_fast16_t)(scale))>>8)

// Events sent to host
#define PLAYER_EVENT_TABLE

#define GAME_STAT_TABLE(X_MACRO) \
	X_MACRO(IDLE, idle) \
	X_MACRO(CONFIGURED, configured) \
	X_MACRO(IN_PROGRESS, in_progress) \
	X_MACRO(TRIG_CONFIRM, trig_confirm) \
	X_MACRO(TRIGGED, trigged) \
	X_MACRO(BLOCKED, blocked) \

#define X_AS_GAME_STAT_ENUM(u_name, l_name) G_STAT_ ## u_name,
enum game_stat {
	GAME_STAT_TABLE(X_AS_GAME_STAT_ENUM)
	__G_STAT_MAX
};

#define X_AS_STAT_NAME(u_name, l_name) #u_name,
static const char * const stat_name[__G_STAT_MAX] = {
	GAME_STAT_TABLE(X_AS_STAT_NAME)
};

#define X_AS_EVENT_NAME(u_name, l_name) #u_name,
static const char *event_name[__CTRL_EVENT_MAX] = {
	CTRL_EVENT_TABLE(X_AS_EVENT_NAME)
};

#define X_AS_HOST_MSG_NAME(u_name, l_name) #u_name,
static const char *host_msg_name[__HOST_MSG_MAX] = {
	HOST_MSG_TABLE(X_AS_HOST_MSG_NAME)
};

#define X_AS_EVENT_PARSER_PROTO(u_name, l_name) \
	static bool parse_ ## l_name(struct ctrl_event*);
CTRL_EVENT_TABLE(X_AS_EVENT_PARSER_PROTO);

#define X_AS_EVENT_PARSER_JUMP_TABLE(u_name, l_name) parse_ ## l_name,
bool (*parse[__CTRL_EVENT_MAX])(struct ctrl_event*) = {
	CTRL_EVENT_TABLE(X_AS_EVENT_PARSER_JUMP_TABLE)
};

static struct {
	enum game_stat stat;
	uint8_t bssid[6];
	struct yam_pixel color;
	enum cfg_color color_idx;
	ip4_addr_t server_addr;
	struct udp_pcb *udp;
	struct ctrl_host_msg msg;
	QueueHandle_t q;
	xTimerHandle t;
	struct yam_cmd_req *yam;
	enum cfg_intensity cfg_intensity;
	enum cfg_intensity bar_intensity;
} game = {};

// Color definitions for the supported colors and the undefined value
static const struct yam_pixel colors[__CFG_COLOR_MAX + 1] = {
	{.r = 255},
	{.g = 255},
	{.b = 255},
	{.r = 255, .g = 255},
	{.r = 255, .b = 255}
};

// Values on indexes 1 to 5 are computed as:
// intensity[i] = 255 ^ (i/5)
static const uint_fast8_t intensity[__CFG_INTENSITY_MAX] = {
	0, 3, 9, 28, 84, 255
};

static void intensity_apply(struct yam_pixel *pixel, uint_fast8_t intensity)
{
	pixel->r = SCALE_U8(pixel->r, intensity);
	pixel->g = SCALE_U8(pixel->g, intensity);
	pixel->b = SCALE_U8(pixel->b, intensity);
}

static void yam_mode_change(struct yam_cmd_req *req)
{
	yam_command(req);
	free(game.yam);
	game.yam = req;
}

static void yam_breath_cmd(void)
{
	struct yam_cmd_req *req = calloc(1, sizeof(struct yam_cmd_req));

	req->mode = YAM_CMD_BREATH;
	req->breath.delay_ms = 32;

	for (int i = 0; i < YAM_PIXEL_NUM; i++) {
		req->breath.pixels[i] = game.color;
	}

	yam_mode_change(req);
}

static void yam_wait_press_cmd(void)
{
	struct yam_cmd_req *req = calloc(1, sizeof(struct yam_cmd_req));

	req->mode = YAM_CMD_PINGPONG;
	req->pingpong.initial_value = game.color;
	req->pingpong.steps = 10;
	req->pingpong.delay_ms = 32;

	yam_mode_change(req);
}

static void yam_wait_start_cmd(void)
{
	struct yam_cmd_req *req = calloc(1, sizeof(struct yam_cmd_req));

	req->mode = YAM_CMD_SLIDE;
	req->slide.delay_ms = 50;
	struct yam_pixel color = game.color;

	for (int i = 0; i < YAM_PIXEL_NUM; i++) {
		req->slide.pixels[i] = color;
		color.r >>= 1;
		color.g >>= 1;
		color.b >>= 1;
	}

	yam_mode_change(req);
}

static void yam_request_refresh(void)
{
	// Re-apply current effect
	switch (game.yam->mode) {
	case YAM_CMD_BREATH:
		yam_breath_cmd();
		break;

	case YAM_CMD_PINGPONG:
		yam_wait_press_cmd();
		break;

	case YAM_CMD_SLIDE:
		yam_wait_start_cmd();
		break;

	default:
		// Ignore unused modes
		break;
	}
}

#define stat_change(st) do { \
	LOGD("%s ==> %s", stat_name[game.stat], stat_name[st]); \
	game.stat = (st); \
} while (0)

static void host_msg_send(enum ctrl_host_msg_type event, bool confirm)
{
	struct pbuf *buf = pbuf_alloc(PBUF_TRANSPORT,
			sizeof(struct ctrl_host_msg), PBUF_RAM);
	struct ctrl_host_msg msg;
	msg.event = event;
	memcpy(msg.bssid, game.bssid, 6);
	memcpy(buf->payload, &msg, sizeof(struct ctrl_host_msg));

	udp_sendto(game.udp, buf, &game.server_addr, HOST_PORT);
	pbuf_free(buf);
	// If command requires confirmation, start retry timer
	if (confirm) {
		xTimerStart(game.t, pdMS_TO_TICKS(5000));
	} else {
		xTimerStop(game.t, pdMS_TO_TICKS(5000));
	}
	LOGD("send to host %s", host_msg_name[event]);
}

static void host_msg_confirm(void)
{
	xTimerStop(game.t, pdMS_TO_TICKS(5000));
	LOGD("command complete");
}

static bool parse_start(struct ctrl_event *ctrl)
{
	bool parsed = true;

	// Ensure color is changed properly
	parse_color(ctrl);

	switch (game.stat) {
	case G_STAT_CONFIGURED:
		// fallthrough
	case G_STAT_TRIGGED:
		// fallthrough
	case G_STAT_BLOCKED:
		// fallthrough
	case G_STAT_TRIG_CONFIRM:
		LOGD("player %d starting", ctrl->data.player_num);
		stat_change(G_STAT_IN_PROGRESS);
		yam_wait_press_cmd();
		// fallthrough
	case G_STAT_IN_PROGRESS:
		gpio_set_level(LED_PIN, LED_OFF);
		host_msg_send(HOST_MSG_START_ACK, false);
		break;

	default:
		parsed = false;
	}

	return parsed;
}

static bool parse_block(struct ctrl_event *ctrl)
{
	UNUSED_PARAM(ctrl);
	bool parsed = true;

	switch (game.stat) {
	case G_STAT_IN_PROGRESS:
		// fallthrough
	case G_STAT_TRIG_CONFIRM:
		stat_change(G_STAT_BLOCKED);
		// fallthrough
	case G_STAT_BLOCKED:
		host_msg_send(HOST_MSG_BLOCK_ACK, false);
		break;

	default:
		parsed = false;
	}

	return parsed;
}

static bool parse_trigger_ack(struct ctrl_event *ctrl)
{
	UNUSED_PARAM(ctrl);
	bool parsed = false;

	if (G_STAT_TRIG_CONFIRM == game.stat) {
		host_msg_confirm();
		stat_change(G_STAT_TRIGGED);
		gpio_set_level(LED_PIN, LED_ON);
		yam_breath_cmd();
		parsed = true;
	}

	return parsed;
}

static bool parse_color(struct ctrl_event *ctrl)
{
	enum cfg_color color_idx = MIN(ctrl->data.led.color, __CFG_COLOR_MAX);
	game.cfg_intensity = game.bar_intensity = MIN(ctrl->data.led.intensity,
			CFG_INTENSITY_100);

	game.color_idx = color_idx;
	game.color = colors[color_idx];
	intensity_apply(&game.color, intensity[game.cfg_intensity]);

	yam_request_refresh();

	return true;
}

static bool parse_button(struct ctrl_event *ctrl)
{
	UNUSED_PARAM(ctrl);
	bool parsed = false;

	if (G_STAT_IN_PROGRESS == game.stat) {
		stat_change(G_STAT_TRIG_CONFIRM);
		host_msg_send(HOST_MSG_TRIGGER, true);
		parsed = true;
	}

	return parsed;
}

static bool parse_ip_get(struct ctrl_event *ctrl)
{
	game.server_addr = ctrl->data.server_ip;
	if (G_STAT_IDLE == game.stat) {
		stat_change(G_STAT_CONFIGURED);
		yam_wait_start_cmd();
	}

	LOGI("configured host on %s:" STR(HOST_PORT),
			ip_ntoa(&game.server_addr));
	return true;
}

static bool parse_battery_level(struct ctrl_event *ctrl)
{
	const struct ctrl_battery_level *level = &ctrl->data.battery_level;

	enum ctrl_host_msg_type msg;

	if (BAT_CHARGING == level->status) {
		if (BAT_EV_LOW == level->event) {
			msg = HOST_MSG_BATT_LOW_CHARGE;
		} else if (BAT_EV_MID == level->event) {
			msg = HOST_MSG_BATT_MID_CHARGE;
		} else {
			msg = HOST_MSG_BATT_HIGH_CHARGE;
		}
	} else {
		if (BAT_EV_LOW == level->event) {
			msg = HOST_MSG_BATT_LOW_DISCHARGE;
		} else if (BAT_EV_MID == level->event) {
			msg = HOST_MSG_BATT_MID_DISCHARGE;
		} else {
			msg = HOST_MSG_BATT_HIGH_DISCHARGE;
		}
	}
	host_msg_send(msg, false);

	if (BAT_EV_LOW == level->event && BAT_DISCHARGING == level->status &&
			game.bar_intensity != CFG_INTENSITY_20) {
		// Set intensity to 20% to signal low battery level
		game.bar_intensity = CFG_INTENSITY_20;

		game.color = colors[game.color_idx];
		intensity_apply(&game.color, intensity[game.bar_intensity]);
		yam_request_refresh();
	} else {
		// Restore configured intensity when batt has charged enough
		if (game.cfg_intensity != game.bar_intensity) {
			game.bar_intensity = game.cfg_intensity;

			game.color = colors[game.color_idx];
			intensity_apply(&game.color,
					intensity[game.bar_intensity]);
			yam_request_refresh();
		}
	}
	return true;
}

static void process(struct ctrl_event *ev)
{
	if (ev->event < 0 || ev->event >= __CTRL_EVENT_MAX) {
		LOGE("ignoring unknown event 0x%X", ev->event);
		return;
	}
	LOGD("%s", event_name[ev->event]);

	bool processed = parse[ev->event](ev);

	if (!processed) {
		LOGW("ignoring event %s on stat %s", event_name[ev->event],
				stat_name[game.stat]);
	}
}

static void player_cmd_retry(void)
{
	// Dunno why, we must copy the buffer or sometimes udp_send() crashes
	// With this in mind, maybe we could save only the payload instead of
	// the full buffer for retries
	struct pbuf *buf = pbuf_alloc(PBUF_TRANSPORT,
			sizeof(struct ctrl_host_msg), PBUF_RAM);
	memcpy(buf->payload, &game.msg, sizeof(struct ctrl_host_msg));
	udp_sendto(game.udp, buf, &game.server_addr, HOST_PORT);
	pbuf_free(buf);
	LOGW("retry command");
}

static void IRAM_ATTR timer_cb(xTimerHandle t)
{
	UNUSED_PARAM(t);

	// Check if we have to retry any of the events
	switch (game.stat) {
	case G_STAT_TRIG_CONFIRM:
		player_cmd_retry();
		break;

	default:
		LOGE("ignoring timer");
		break;
	}
}

static void ctrl_tsk(void *arg)
{
	UNUSED_PARAM(arg);
	struct ctrl_event event;

	while(true) {
		if (pdTRUE == xQueueReceive(game.q, &event,
					pdMS_TO_TICKS(1000))) {
			process(&event);
		}
	}
}

int ctrl_start(void)
{
	int err = 1;

	game.q = xQueueCreate(10, sizeof(struct ctrl_event));
	if (0 == game.q) {
		LOGE("queue creation failed");
		goto error;
	}

	if (pdPASS != xTaskCreate(ctrl_tsk, "ctrl_tsk", CTRL_TASK_STACK,
				NULL, CTRL_TASK_PRIO, NULL)) {
		LOGE("task creation failed");
		goto error;
	}

	return 0;

error:
	if (game.q) {
		vQueueDelete(game.q);
		game.q = 0;
	}

	return err;
}

int ctrl_event(const struct ctrl_event *event)
{
	if (pdTRUE != xQueueSend(game.q, event, pdMS_TO_TICKS(5000))) {
		LOGE("failed to send event %d", event->event);
		return -1;
	}

	return 0;
}

void ctrl_event_from_isr(const struct ctrl_event *event)
{
	BaseType_t context_switch = pdFALSE;

	xQueueSendFromISR(game.q, event, &context_switch);
	if (context_switch) {
		taskYIELD();
	}
}

static void led_cfg(void)
{
	static const gpio_config_t cfg = {
		.mode = GPIO_MODE_OUTPUT,
		.pin_bit_mask = LED_PIN_MASK
	};

	gpio_config(&cfg);
}

void ctrl_init(struct udp_pcb *socket, const uint8_t bssid[6])
{
	memset(&game, 0, sizeof(game));
	memcpy(game.bssid, bssid, 6);

	game.color_idx = CFG_COLOR_DEFAULT;
	game.color = colors[CFG_COLOR_DEFAULT];
	game.cfg_intensity = game.bar_intensity = CFG_INTENSITY_DEFAULT;
	intensity_apply(&game.color, intensity[game.cfg_intensity]);
	led_cfg();
	gpio_set_level(LED_PIN, LED_OFF);
	yam_breath_cmd();

	game.t = xTimerCreate(NULL, RETRY_TOUT_MS, pdTRUE, NULL, timer_cb);
	game.udp = socket;
}

