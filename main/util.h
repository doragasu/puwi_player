#ifndef _UTIL_H_
#define _UTIL_H_

#include <esp_log.h>

/// Stringify token, helper macro
#define _STR(x)		#x
/// Stringify token
#define STR(x)		_STR(x)

/// Remove compiler warnings when not using a function parameter
#define UNUSED_PARAM(x)		(void)x

/// Puts a task to sleep for some milliseconds (requires FreeRTOS).
#define vTaskDelayMs(ms)	vTaskDelay((ms)/portTICK_PERIOD_MS)

#if !defined(MAX)
/// Returns the maximum of two numbers
#define MAX(a, b)	((a)>(b)?(a):(b))
#endif
#if !defined(MIN)
/// Returns the minimum of two numbers
#define MIN(a, b)	((a)<(b)?(a):(b))
#endif

#define LOGE(...) ESP_LOGE(__func__, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(__func__, __VA_ARGS__)
#define LOGI(...) ESP_LOGI(__func__, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(__func__, __VA_ARGS__)

void *memdup(const void *mem, size_t len);

#endif //_UTIL_H_

