#include <string.h>
#include <esp_timer.h>
#include <nvs_flash.h>
#include <FreeRTOS.h>
#include <driver/gpio.h>

#include <yamcha.h>

#include "util.h"
#include "wifi_sta.h"
#include "net_ev.h"
#include "battery.h"
#include "control.h"

#define PUWI_PLAYER_VERSION "1.0"

#define PROG_PIN 0
#define PROG_PIN_MASK (1UL<<PROG_PIN)

#define INPUT_PIN 13
#define INPUT_PIN_MASK (1UL<<INPUT_PIN)

#define NEOPIXEL_PIN 12

// 150 ms debounce time
#define DEBOUNCE_US	150000

static IRAM_ATTR void button_event_isr(void *arg)
{
	static uint64_t last = 0;
	uint64_t current = esp_timer_get_time();

	if ((current - last) > DEBOUNCE_US) {
		struct ctrl_event ctrl = {
			.event = CTRL_EVENT_BUTTON,
			.data.gpio = (int)arg
		};
		ctrl_event_from_isr(&ctrl);
		last = current;
	}
}

static void sta_event_cb(enum wifi_event event, union wifi_event_data *data)
{
	struct ctrl_event ev;

	switch (event) {
	case WIFI_EVENT_GOT_IP:
		ev.event = CTRL_EVENT_IP_GET;
		ev.data.server_ip = data->got_ip.ip_info.gw;
		ctrl_event(&ev);
		break;

	default:
		// Assoc and disassoc events are ignored
		break;
	}
}

void net_event_cb(const ip4_addr_t *ip, uint_fast16_t port,
		void *data, uint_fast16_t data_len)
{
	UNUSED_PARAM(ip);
	UNUSED_PARAM(port);
	struct ctrl_event ev;

	if (data_len > sizeof(struct ctrl_event)) {
		LOGW("unexpected packet length %d", data_len);
		return;
	}

	// We have to copy data because lwIP pbuffers are byte aligned
	memcpy(&ev, data, data_len);

	// TODO: do not assume we receive from host IP
	LOGD("got event: 0x%X", ev.event);
	ctrl_event(&ev);
}

static void button_cfg(void)
{
	static const gpio_config_t cfg = {
		.mode = GPIO_MODE_INPUT,
		.intr_type = GPIO_INTR_NEGEDGE,
		.pull_up_en = GPIO_PULLUP_ENABLE,
		.pin_bit_mask = INPUT_PIN_MASK | PROG_PIN_MASK
	};

	gpio_config(&cfg);
	gpio_install_isr_service(0);
	gpio_isr_handler_add(INPUT_PIN, button_event_isr, (void*)INPUT_PIN);
	gpio_isr_handler_add(PROG_PIN, button_event_isr, (void*)PROG_PIN);
}

static void battery_event_cb(enum bat_event event, enum bat_status status,
		uint32_t voltage_millis)
{
	struct ctrl_event ctrl = {
		.event = CTRL_EVENT_BATTERY_LEVEL
	};

	ctrl.data.battery_level.event = event;
	ctrl.data.battery_level.status = status;
	ctrl.data.battery_level.voltage_millis = voltage_millis;

	ctrl_event(&ctrl);
}

void app_main(void)
{
	const struct bat_cfg bat_cfg = {
		.cb = battery_event_cb,
		.threshold = BAT_THRESHOLD_DEFAULT,
		.hysteresis = 50
	};
	uint8_t bssid[6];

	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	LOGI("STARTING PUWI PLAYER VERSION " PUWI_PLAYER_VERSION);

	yam_init(NEOPIXEL_PIN);
	button_cfg();
	bat_start(&bat_cfg);
	wifi_init_sta(sta_event_cb);
	ESP_ERROR_CHECK(esp_wifi_get_mac(ESP_IF_WIFI_STA, bssid));
	ESP_ERROR_CHECK(esp_wifi_set_ps(WIFI_PS_NONE));
	struct udp_pcb *udp = udp_new();
	ctrl_init(udp, bssid);
	ESP_ERROR_CHECK(ctrl_start());
	nev_init(udp, net_event_cb);
}

