#include <string.h>
#include <esp_event_loop.h>
#include <esp_wifi.h>

#include "wifi_sta.h"
#include "util.h"

#define EXAMPLE_ESP_WIFI_SSID      CONFIG_ESP_WIFI_SSID
#define EXAMPLE_ESP_WIFI_PASS      CONFIG_ESP_WIFI_PASSWORD

static wifi_event_cb event_cb = NULL;

static void disconnected_proc(system_event_sta_disconnected_t *dis)
{
	union wifi_event_data data;

	LOGE("reason: %d", dis->reason);
	if (dis->reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT) {
		// switch to 802.11 bgn mode
		esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCOL_11B |
				WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N);
	}
	esp_wifi_connect();

	if (event_cb) {
		memcpy(data.disassoc.bssid, dis->bssid, 6);
		event_cb(WIFI_EVENT_DISASSOC, &data);
	}
}

static void connected_proc(system_event_sta_connected_t *con)
{
	union wifi_event_data data;

	LOGI("BSSID: " MACSTR, MAC2STR(con->bssid));
	if (event_cb) {
		memcpy(data.assoc.bssid, con->bssid, 6);
		event_cb(WIFI_EVENT_ASSOC, &data);
	}
}

static void got_ip_proc(system_event_sta_got_ip_t *got_ip)
{
	union wifi_event_data data;

	LOGI("IPv4: %s, gw: %s", ip_ntoa(&got_ip->ip_info.ip),
			ip_ntoa(&got_ip->ip_info.gw));
	if (event_cb) {
		data.got_ip.ip_info = got_ip->ip_info;
		event_cb(WIFI_EVENT_GOT_IP, &data);
	}
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
	UNUSED_PARAM(ctx);
	system_event_info_t *info = &event->event_info;

	switch(event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;

	case SYSTEM_EVENT_STA_CONNECTED:
		connected_proc(&info->connected);
		break;

	case SYSTEM_EVENT_STA_GOT_IP:
		got_ip_proc(&info->got_ip);
		break;

	case SYSTEM_EVENT_STA_DISCONNECTED:
		disconnected_proc(&info->disconnected);
		break;

	default:
		break;
	}

	return ESP_OK;
}

void wifi_init_sta(wifi_event_cb _event_cb)
{
	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	wifi_config_t wifi_config = {
		.sta = {
			.ssid = EXAMPLE_ESP_WIFI_SSID,
			.password = EXAMPLE_ESP_WIFI_PASS,
			.threshold.authmode = WIFI_AUTH_WPA2_PSK
		},
	};

	event_cb = _event_cb;

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
	ESP_ERROR_CHECK(esp_wifi_start());

	LOGI("connecting to ap SSID: %s", EXAMPLE_ESP_WIFI_SSID);
}

