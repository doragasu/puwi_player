#include <stdlib.h>
#include <string.h>

#include "util.h"

void *memdup(const void *mem, size_t len)
{
	void *dup = NULL;
	
	if (mem && len) {
		dup = malloc(len);
	}

	if (dup) {
		memcpy(dup, mem, len);
	}

	return dup;
}

